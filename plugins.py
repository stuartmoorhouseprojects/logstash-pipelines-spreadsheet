import requests
import logging
import json
import os
import csv

logging.basicConfig(filename="plugins.log",
                    filemode='a',
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.DEBUG)

testPlugins =    [ ["logstash-codec-avro","codec","Elastic"]]

plugins =    [ ["logstash-codec-avro","codec","Elastic"] , ["logstash-codec-cef","codec","Elastic"] , ["logstash-codec-cloudfront","codec","Community"] , ["logstash-codec-cloudtrail","codec","Community"] , ["logstash-codec-collectd","codec","Elastic"] , ["logstash-codec-compress_spooler","codec","Community"] , ["logstash-codec-dots","codec","Elastic"] , ["logstash-codec-edn","codec","Community"] , ["logstash-codec-edn_lines","codec","Community"] , ["logstash-codec-es_bulk","codec","Elastic"] , ["logstash-codec-fluent","codec","Elastic"] , ["logstash-codec-graphite","codec","Elastic"] , ["logstash-codec-gzip_lines","codec","Community"] , ["logstash-codec-json","codec","Elastic"] , ["logstash-codec-json_lines","codec","Elastic"] , ["logstash-codec-line","codec","Elastic"] , ["logstash-codec-msgpack","codec","Elastic"] , ["logstash-codec-multiline","codec","Elastic"] , ["logstash-codec-netflow","codec","Elastic"] , ["logstash-codec-nmap","codec","Community"] , ["logstash-codec-oldlogstashjson","codec","Community"] , ["logstash-codec-plain","codec","Elastic"] , ["logstash-codec-rubydebug","codec","Elastic"] , ["logstash-codec-s3plain","codec","Community"] , ["logstash-codec-syslog","codec","Elastic"] , ["logstash-codec-spool","codec","Community"] , ["logstash-filter-age","filter","Community"] , ["logstash-filter-aggregate","filter","Elastic"] , ["logstash-filter-alter","filter","Community"] , ["logstash-filter-checksum","filter","Propose to Delete"] , ["logstash-filter-anonymize","filter","Delete"] , ["logstash-filter-cidr","filter","Elastic"] , ["logstash-filter-cipher","filter","Community"] , ["logstash-filter-clone","filter","Elastic"] , ["logstash-filter-collate","filter","Community"] , ["logstash-filter-csv","filter","Elastic"] , ["logstash-filter-date","filter","Elastic"] , ["logstash-filter-de_dot","filter","Elastic"] , ["logstash-filter-dissect","filter","Elastic"] , ["logstash-filter-dns","filter","Elastic"] , ["logstash-filter-drop","filter","Elastic"] , ["logstash-filter-elapsed","filter","Community"] , ["logstash-filter-Elasticsearch","filter","Elastic"] , ["logstash-filter-emoji","filter","Community"] , ["logstash-filter-environment","filter","Community"] , ["logstash-filter-extractnumbers","filter","Community"] , ["logstash-filter-fingerprint","filter","Elastic"] , ["logstash-filter-geoip","filter","Elastic"] , ["logstash-filter-grok","filter","Elastic"] , ["logstash-filter-http","filter","Elastic"] , ["logstash-filter-i18n","filter","Community"] , ["logstash-filter-jdbc_static","filter","Elastic"] , ["logstash-filter-jdbc_streaming","filter","Elastic"] , ["logstash-filter-json","filter","Elastic"] , ["logstash-filter-json_encode","filter","Community"] , ["logstash-filter-kubernetes_metadata","filter","Community"] , ["logstash-filter-kv","filter","Elastic"] , ["logstash-filter-language","filter","Community"] , ["logstash-filter-memcached","filter","Elastic"] , ["logstash-filter-metaevent","filter","Community"] , ["logstash-filter-metricize","filter","Community"] , ["logstash-filter-metrics","filter","Community"] , ["logstash-filter-multiline","filter","Community"] , ["logstash-filter-mutate","filter","Elastic"] , ["logstash-filter-oui","filter","Community"] , ["logstash-filter-prune","filter","Elastic"] , ["logstash-filter-punct","filter","Community"] , ["logstash-filter-range","filter","Community"] , ["logstash-filter-ruby**","filter","Elastic"] , ["logstash-filter-sleep","filter","Elastic"] , ["logstash-filter-split","filter","Elastic"] , ["logstash-filter-syslog_pri","filter","Elastic"] , ["logstash-filter-throttle","filter","Elastic"] , ["logstash-filter-tld","filter","Community"] , ["logstash-filter-translate","filter","Elastic"] , ["logstash-filter-truncate","filter","Elastic"] , ["logstash-filter-unique","filter","Community"] , ["logstash-filter-urldecode","filter","Elastic"] , ["logstash-filter-useragent","filter","Elastic"] , ["logstash-filter-uuid","filter","Elastic"] , ["logstash-filter-xml","filter","Elastic"] , ["logstash-filter-yaml","filter","Community"] , ["logstash-filter-zeromq","filter","Community"] , ["logstash-input-azure_event_hubs","input","Elastic"] , ["logstash-input-beats","input","Elastic"] , ["logstash-input-cloudwatch","input","Community"] , ["logstash-input-couchdb_changes","input","Elastic"] , ["logstash-input-dead_letter_queue","input","Elastic"] , ["logstash-input-drupal_dblog","input","Community"] , ["logstash-input-dynamodb","input","Community"] , ["logstash-input-Elastic_agent","input","Elastic"] , ["logstash-input-Elasticsearch","input","Elastic"] , ["logstash-input-Elastic_serverless_forwarder","input","Elastic"] , ["logstash-input-eventlog","input","Community"] , ["logstash-input-exec","input","Community"] , ["logstash-input-file","input","Elastic"] , ["logstash-input-fluentd","input","Community"] , ["logstash-input-ganglia","input","Community"] , ["logstash-input-gelf","input","Elastic"] , ["logstash-input-gemfire","input","Community"] , ["logstash-input-generator","input","Elastic"] , ["logstash-input-github","input","Community"] , ["logstash-input-google_pubsub","input","Community"] , ["logstash-input-graphite","input","Elastic"] , ["logstash-input-heartbeat","input","Elastic"] , ["logstash-input-heroku","input","Community"] , ["logstash-input-http","input","Elastic"] , ["logstash-input-http_poller","input","Elastic"] , ["logstash-input-imap","input","Community"] , ["logstash-input-jms","input","Propose to Delete"] , ["logstash-input-irc","input","Community"] , ["logstash-input-jdbc","input","Elastic"] , ["logstash-input-jms***","input","Elastic"] , ["logstash-input-jmx","input","Community"] , ["logstash-input-kafka","input","Elastic"] , ["logstash-input-kinesis","input","Community"] , ["logstash-input-log4j","input","Elastic"] , ["logstash-input-log4j2","input","Community"] , ["logstash-input-lumberjack","input","Community"] , ["logstash-input-meetup","input","Community"] , ["logstash-input-neo4j","input","Community"] , ["logstash-input-perfmon","input","Community"] , ["logstash-input-pipe","input","Community"] , ["logstash-input-puppet_facter","input","Community"] , ["logstash-input-rabbitmq","input","Elastic"] , ["logstash-input-rackspace","input","Community"] , ["logstash-input-redis","input","Elastic"] , ["logstash-input-relp","input","Community"] , ["logstash-input-rss","input","Community"] , ["logstash-input-s3*","input","Elastic"] , ["logstash-input-salesforce","input","Community"] , ["logstash-input-snmp","input","Elastic"] , ["logstash-input-snmptrap","input","Community"] , ["logstash-input-sqlite","input","Community"] , ["logstash-input-sqs","input","Elastic"] , ["logstash-input-stdin","input","Elastic"] , ["logstash-input-stomp","input","Community"] , ["logstash-input-syslog","input","Elastic"] , ["logstash-input-tcp","input","Elastic"] , ["logstash-input-twitter","input","Elastic"] , ["logstash-input-udp","input","Elastic"] , ["logstash-input-unix","input","Community"] , ["logstash-input-varnishlog","input","Community"] , ["logstash-input-websocket","input","Community"] , ["logstash-input-wmi","input","Community"] , ["logstash-input-xmpp","input","Community"] , ["logstash-input-zenoss","input","Community"] , ["logstash-input-zeromq","input","Community"] , ["logstash-output-boundary","output","Community"] , ["logstash-output-circonus","output","Community"] , ["logstash-output-cloudwatch","output","Community"] , ["logstash-output-csv","output","Elastic"] , ["logstash-output-Elasticsearch_http","output","Propose to Delete"] , ["logstash-output-datadog","output","Community"] , ["logstash-output-datadog_metrics","output","Community"] , ["logstash-output-Elastic_app_search","output","Elastic"] , ["logstash-output-Elasticsearch","output","Elastic"] , ["logstash-output-Elasticsearch_java","output","Community"] , ["logstash-output-Elasticsearch-ec2","output","Community"] , ["logstash-output-Elastic_workplace_search","output","Elastic"] , ["logstash-output-email","output","Elastic"] , ["logstash-output-exec","output","Community"] , ["logstash-output-file","output","Elastic"] , ["logstash-output-ganglia","output","Community"] , ["logstash-output-gelf","output","Community"] , ["logstash-output-gemfire","output","Community"] , ["logstash-output-google_bigquery","output","Community"] , ["logstash-output-google_cloud_storage","output","Community"] , ["logstash-output-graphite","output","Elastic"] , ["logstash-output-graphtastic","output","Community"] , ["logstash-output-hipchat","output","Community"] , ["logstash-output-http","output","Elastic"] , ["logstash-output-jms","output","Propose to Delete"] , ["logstash-output-influxdb","output","Community"] , ["logstash-output-irc","output","Community"] , ["logstash-output-jira","output","Community"] , ["logstash-output-jms","output","Community"] , ["logstash-output-juggernaut","output","Community"] , ["logstash-output-kafka","output","Elastic"] , ["logstash-output-librato","output","Community"] , ["logstash-output-logentries","output","Community"] , ["logstash-output-loggly","output","Community"] , ["logstash-output-lumberjack","output","Elastic"] , ["logstash-output-metriccatcher","output","Community"] , ["logstash-output-monasca_log_api","output","Community"] , ["logstash-output-mongodb","output","Community"] , ["logstash-output-nagios","output","Community"] , ["logstash-output-null","output","Propose to Delete"], ["logstash-output-nagios_nsca","output","Community"] , ["logstash-output-neo4j","output","Community"] , ["logstash-output-newrelic","output","Community"] , ["logstash-output-opentsdb","output","Community"] , ["logstash-output-pagerduty","output","Community"] , ["logstash-output-pipe","output","Community"] , ["logstash-output-rabbitmq","output","Elastic"] , ["logstash-output-rackspace","output","Community"] , ["logstash-output-redis","output","Elastic"] , ["logstash-output-redmine","output","Community"] , ["logstash-output-riak","output","Community"] , ["logstash-output-riemann","output","Community"] , ["logstash-output-s3*","output","Elastic"] , ["logstash-output-slack","output","Community"] , ["logstash-output-sns","output","Community"] , ["logstash-output-solr_http","output","Community"] , ["logstash-output-sqs","output","Community"] , ["logstash-output-statsd","output","Community"] , ["logstash-output-stdout","output","Elastic"] , ["logstash-output-stomp","output","Community"] , ["logstash-output-syslog","output","Community"] , ["logstash-output-tcp","output","Elastic"] , ["logstash-output-udp","output","Elastic"] , ["logstash-output-webhdfs","output","Community"] , ["logstash-output-websocket","output","Community"] , ["logstash-output-xmpp","output","Community"] , ["logstash-output-zabbix","output","Community"] , ["logstash-output-zeromq","output","Community"] , ["logstash-output-zookeeper","output","Community"] ]

baseUrl = "https://api.github.com/repos/logstash-plugins/"

_authorizationToken = os.environ.get('PLUGINS_PY_AUTH')

authorizationToken = "Bearer " + _authorizationToken
headers = {"Authorization": authorizationToken}

urls = []

for plugin in plugins:
    url = baseUrl + plugin[0] + "/commits"
    logging.debug("url = " + url)
    plugin.insert(1, url)

    response = requests.get(url,  headers=headers)
    commits = response.json()

    if response.status_code == 200:
      commitDates = []

      for commit in commits:
         logging.debug("commit = " + json.dumps(commit))
         date = commit["commit"]["author"]["date"]
         commitDates.append(date)

      firstCommit = commitDates[0]
      lastCommit = commitDates[-1]
      plugin.append(lastCommit)
      plugin.append(firstCommit)

    else:
      plugin.append(response.status_code)
      plugin.append(response.status_code) 
    
  
try:
    os.remove('logstash-pipelines.csv')
except OSError:
    pass

with open('logstash-pipelines.csv', 'w') as f:
    csv.writer(f, delimiter=',').writerows(plugins)






